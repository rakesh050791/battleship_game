class Player
  attr_reader :board

  attr_accessor :name, :ships, :id

  def initialize(name)
    @name = name
    @board = Board.new(name: self.name, size: 10)
    @ships = create_ships
  end

  def create_ships
    {
        p: Ship.p,
        q: Ship.q
    }
  end
end