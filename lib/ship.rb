class Ship
  SIZES = {
      p: 1,
      q: 2
  }

  attr_reader :type, :size

  def initialize type
    @type = type.to_sym
    @size = SIZES[@type]
    @hits = 0
  end

  def hit
    @hits += 1
  end

  def sunk?
    @hits >= size
  end

  def self.p
    Ship.new(:p)
  end

  def self.q
    Ship.new(:q)
  end
end